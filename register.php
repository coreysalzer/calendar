<?php
   ini_set("session.cookie_httponly", 1);
   session_start();
   
   header("Content-Type: application/json");
 
   $username = isset($_POST['username']) ? filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING) : '';
   $password = isset($_POST['password']) ? filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING) : '';
   $username = trim($username);
   $password = trim($password);
   
   if(isset($_POST['username']) && isset($_POST['password'])) {               
      require 'database.php';              

      $pwd_hash= crypt($password);
            
      if($username != "" and $password != "") {
         $stmt1 = $mysqli->prepare("SELECT COUNT(*) FROM user_information WHERE username=?");
         if(!$stmt1){
         echo json_encode(array(
            "success" => false,
            "message" => "Unable to Access Database"
         ));
         exit;
         }
         $stmt1->bind_param('s', $username);
         $stmt1->execute();
         $stmt1->bind_result($cnt);
         $stmt1->store_result();
         $stmt1->fetch();
         if($cnt === 0) {
             $stmt2 = $mysqli->prepare("INSERT INTO user_information (username, password) VALUE (?, ?)");
             if(!$stmt2){
               echo json_encode(array(
                  "success" => false,
                  "message" => "Unable to Access Database"
               ));
               exit;
             }
             $stmt2->bind_param('ss', $username, $pwd_hash);
             $stmt2->execute();
         }
         else {
            echo json_encode(array(
                "success" => false,
                "message" => "The username you entered has already been chosen. Please enter a different one"
                ));
                exit;
         }
      }
      else {
         echo json_encode(array(
            "success" => false,
            "message" => "Incorrect Username or Password"
	));
         exit;
      }
      echo json_encode(array(
            "success" => true,
            "message" => "You are now registered",
            "username" => $username
	));
      exit;
   }
   
?>

