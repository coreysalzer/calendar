<?php
   ini_set("session.cookie_httponly", 1);
   session_start();
   
   header("Content-Type: application/json");
 
   $username = isset($_POST['username']) ? filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING) : '';
   $password = isset($_POST['password']) ? filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING) : '';
   $username = trim($username);
   $password = trim($password);
   
   if(isset($_POST['username']) && isset($_POST['password'])) {               
      require 'database.php';              

      $stmt = $mysqli->prepare("SELECT COUNT(*), id, password FROM user_information WHERE username=?");
      if(!$stmt){
            echo json_encode(array(
               "success" => false,
               "message" => "Unable to Access Database"
            ));
            exit;
      }
      $stmt->bind_param('s', $username);
      $stmt->execute();
      $stmt->bind_result($cnt, $user_id, $pwd_hash);
      $stmt->fetch();
      $stmt->close();
      
      if($cnt === 1 && crypt($password, $pwd_hash)===$pwd_hash && $username != "") {
         $_SESSION['user_id'] = $user_id;
         $_SESSION['username'] = $username;
         $_SESSION['token'] = substr(md5(rand()), 0, 10);
         echo json_encode(array(
            "success" => true,
            "username" => $username,
	    "token" => $_SESSION['token']
         ));
         exit;
      }
      else {
         echo json_encode(array(
            "success" => false,
            "message" => "Incorrect Username or Password"
	));
      }
   }
?>