<?php
    ini_set("session.cookie_httponly", 1);
    session_start();
    
    header("Content-Type: application/json");
    
    $username = $_SESSION['username'];
    $event_name = isset($_POST['event_name']) ? filter_input(INPUT_POST, 'event_name', FILTER_SANITIZE_STRING) : '';
    $start_date = isset($_POST['start_date']) ? filter_input(INPUT_POST, 'start_date', FILTER_SANITIZE_STRING) : '';
    $end_date = isset($_POST['end_date']) ? filter_input(INPUT_POST, 'end_date', FILTER_SANITIZE_STRING) : '';
    $start_time = isset($_POST['start_time']) ? filter_input(INPUT_POST, 'start_time', FILTER_SANITIZE_STRING) : '';
    $end_time = isset($_POST['end_time']) ? filter_input(INPUT_POST, 'end_time', FILTER_SANITIZE_STRING) : '';
    $category = isset($_POST['category']) ? filter_input(INPUT_POST, 'category', FILTER_SANITIZE_STRING) : '';
  
    
    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }
    if(isset($_POST['start_date']) && isset($_POST['end_date']) && isset($_POST['start_time'])&& isset($_POST['end_time'])&& isset($_POST['event_name'])) {
        
        require 'database.php';
        
        $start_date .= " " . $start_time . ":00";
        $end_date .= " " . $end_time . ":00";
        
        if($end_date < $start_date){
            //Break if end date is before start date
            exit;
        }
        
        $stmt2 = $mysqli->prepare("SELECT id FROM user_information WHERE username=?");
         if(!$stmt2){
         echo json_encode(array(
           "success" => false,
           "message" => "Unable to Access Database"
         ));
            exit;
         }
         $stmt2->bind_param('s', $username);
         $stmt2->execute();
         $stmt2->bind_result($user_id);
         $stmt2->fetch();
         $stmt2->close();
         
         
         
        $stmt4 = $mysqli->prepare("INSERT INTO Events (start_date, end_date, event_name, category, user_id) VALUE (?, ?, ?, ?, ?)");
         if(!$stmt4){
            echo json_encode(array(
               "success" => false,
               "message" => "Unable to Access Database"
             ));
            exit;
         }
         $stmt4->bind_param('sssss', $start_date, $end_date, $event_name, $category, $user_id);
         $stmt4->execute();
         $stm4->close();
         echo json_encode(array(
           "success" => true
         ));
         exit;
    }

?>
