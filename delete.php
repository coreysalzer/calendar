<?php
    ini_set("session.cookie_httponly", 1);
    session_start();
    
        
    header("Content-Type: application/json");
      
    $id = $_POST['event_id']; //Story ID of deleted story
    
       if($_SESSION['token'] !== $_POST['token']){
            die("Request forgery detected");
       } 
       require 'database.php';
       
       $stmt1 = $mysqli->prepare("DELETE FROM Events WHERE id=?");
       if(!$stmt1){
         echo json_encode(array(
           "success" => false,
           "message" => "Unable to Access Database"
         ));
         exit;
       }
       $stmt1->bind_param('s', $id);
       $stmt1->execute();
       $stmt1->close();
       echo json_encode(array(
           "success" => true
       ));
       exit;
      
?>