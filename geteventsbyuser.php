
<?php
    ini_set("session.cookie_httponly", 1);
    session_start();
    
    header("Content-Type: application/json");
    
    require 'database.php';
    
    $eventnames = array();
    
    $username = $_SESSION['username'];
    
    if($_SESSION['token'] !== $_POST['token']){
        die("Request forgery detected");
    }

    $stmt = $mysqli->prepare("SELECT id FROM user_information WHERE username=?");
         if(!$stmt){
         echo json_encode(array(
           "success" => false,
           "message" => "Unable to Access Database"
         ));
            exit;
         }
         $stmt->bind_param('s', $username);
         $stmt->execute();
         $stmt->bind_result($user_id);
         $stmt->fetch();
         $stmt->close();

    $stmt2 = "SELECT event_name, id FROM Events WHERE user_id=".$user_id;
         $result = $mysqli->query($stmt2);
         while ($row = mysqli_fetch_array($result)){
            $eventnames[] = $row['event_name'];
            $eventids[] = $row['id'];
         }

         
         mysqli_close($mysqli);

            
        echo json_encode(array(
               "success" => true,
               "message" => "This is a message",
               "eventNamesByUser" => $eventnames,
               "eventIdsByUser" => $eventids
        ));
        exit;
?>