
<?php
//Retrieves all events for the month from the server
//Please return information in this format:
//    success: true
//    message: "Example Message"
//    eventnames: An array of event names 
//    startdatetimes: An array of start datetimes
//    enddatetimes: An array of end datetimes
//    categories: An array of categories (elements can be null)
// Note: All arrays returned should be indexed from 0 to n-1
    ini_set("session.cookie_httponly", 1);
    session_start();
    
    header("Content-Type: application/json");
    
    require 'database.php';
    
    $months = array("01","02","03","04","05","06","07","08","09","10","11","12");
    $eventnames = array();
    $startdatetimes = array();
    $enddatetimes = array();
    $categories = array();
    
    $username = $_SESSION['username'];
    //Gives the number of the month i.e. 0 for January, 1 for February, etc.
    $monthNumber = isset($_POST['month']) ? filter_input(INPUT_POST, 'month', FILTER_SANITIZE_STRING) : '';
    $monthNumber = $months[$monthNumber];
    ////Gives the year i.e. 2015
    $year = isset($_POST['year']) ? filter_input(INPUT_POST, 'year', FILTER_SANITIZE_STRING) : '';

    $stmt2 = "SELECT start_date, end_date, event_name, category FROM Events WHERE start_date LIKE '%".$year."-".$monthNumber."%' ORDER BY start_date";
         $result = $mysqli->query($stmt2);
         while ($row = mysqli_fetch_array($result)){
            $startdatetimes[] = $row['start_date'];
            $enddatetimes[] = $row['end_date'];
            $eventnames[] = $row['event_name'];
            $categories[] = $row['category'];
         }

         mysqli_close($mysqli);

            
        echo json_encode(array(
               "success" => true,
               "message" => "This is a message",
               "eventnames" => $eventnames,
               "startdatetimes" => $startdatetimes,
               "enddatetimes" => $enddatetimes,
               "categories" => $categories
        ));
        exit;
?>