<?php
    //Edits a single event

    ini_set("session.cookie_httponly", 1);
    session_start();
    
    header("Content-Type: application/json");
    
    $username = $_SESSION['username'];
    $id = $_POST['event_id']; //Story ID of editing story
    
    $event_name = isset($_POST['event_name']) ? filter_input(INPUT_POST, 'event_name', FILTER_SANITIZE_STRING) : '';
    $start_date = isset($_POST['start_date']) ? filter_input(INPUT_POST, 'start_date', FILTER_SANITIZE_STRING) : '';
    $end_date = isset($_POST['end_date']) ? filter_input(INPUT_POST, 'end_date', FILTER_SANITIZE_STRING) : '';
    $start_time = isset($_POST['start_time']) ? filter_input(INPUT_POST, 'start_time', FILTER_SANITIZE_STRING) : '';
    $end_time = isset($_POST['end_time']) ? filter_input(INPUT_POST, 'end_time', FILTER_SANITIZE_STRING) : '';
    $category = isset($_POST['category']) ? filter_input(INPUT_POST, 'category', FILTER_SANITIZE_STRING) : '';

       
        require 'database.php';
        
        
        $stmt = $mysqli->prepare("SELECT start_date, end_date, event_name, category FROM Events WHERE id=?");
         if(!$stmt){
         echo json_encode(array(
           "success" => false,
           "message" => "Unable to Access Database"
         ));
            exit;
         }
         $stmt->bind_param('s', $id);
         $stmt->execute();
         $stmt->bind_result($edit_start_date, $edit_end_date, $edit_event_name, $edit_category);
         $stmt->fetch();
         $stmt->close();
        

        
        if(isset($_POST['start_date']) && isset($_POST['start_time']) && $_POST['start_date'] != ""){

            
            $start_date .= " " . $start_time . ":00";
            $edit_start_date = $start_date;
        }
        if(isset($_POST['end_date']) && isset($_POST['end_time'])){

            $end_date .= " " . $end_time . ":00";
            $edit_end_date = $end_date;
        }
        if(isset($_POST['event_name'])){
            $edit_event_name = $event_name;
        }
        if(isset($_POST['category'])){
            $edit_category = $category;
        }

        if($edit_end_date < $edit_start_date){
            //Break if end date is before start date
            exit;
        }


        $stmt4 = $mysqli->prepare("UPDATE Events SET start_date=?, end_date=?, event_name=?, category=? WHERE id=?");
         if(!$stmt4){
            echo json_encode(array(
               "success" => false,
               "message" => "Unable to Access Database"
             ));
            exit;
         }
         $stmt4->bind_param('sssss', $edit_start_date, $edit_end_date, $edit_event_name, $edit_category, $id);
         $stmt4->execute();
         $stm4->close();
         echo json_encode(array(
           "success" => true,
           "message" => "Hello world"
         ));
         exit;

?>